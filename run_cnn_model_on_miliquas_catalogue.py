#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 09:43:29 2020

@author: frederic
"""
from os.path import join 
from glob import glob
import numpy as np
from keras.models import load_model 
import matplotlib.pyplot as plt 

N = 7
def loadimage(gripath):
    im = np.load(gripath)[5:-5, 5:-5, :]
    m, s = np.nanmean(im), np.nanstd(im)
    im[im>m+N*s] = m+N*s
    im -= np.nanmin(im)
    im /= np.nanmax(im)
    return im.astype(np.float32)


# modelsave = 'intense_training/model_at_i=10_largeseponly.model'
# modelsave2 = 'model_at_i=16.model'
# modelsave2 = 'high_false_positive_training/model_at_i=9.model'
modelsave2 = "model_only_nice_positives_at_i=0.model"
# modelsave3 = 'model_at_i=32_refined.model'
miliquas_gri_path = '/media/frederic/kingston_data/train_synthetic_quads/miliquas_goal3/0_gri_arrays/'

#=============================================================================
# model = load_model(modelsave)
model2 = load_model(modelsave2)
# model3 = load_model(modelsave3)
all_gri = glob(join(miliquas_gri_path, '*.npy')) 
np.random.shuffle(all_gri)

# we cannot load everything in the memory at once. Do batches of 24000 gri images
batch_size = 24000
n_batches = len(all_gri)//batch_size + 1

scores = []
for i in range(n_batches):
# for i in range(2):
    print(i, n_batches)
    start = i*batch_size
    stop  = min((i+1)*batch_size, len(all_gri))
    ims = []
    for j in range(start, stop):
        ims.append(loadimage(all_gri[j]))
    ims = np.array(ims) 
    ims[np.where(np.isnan(ims))] = 0.1
    # yp = model.predict(ims)
    # yp = yp.flatten()
    yp2 = model2.predict(ims)
    yp2 = yp2.flatten()
    # yp3 = model3.predict(ims)
    # yp3 = yp3.flatten()
    # yp = 0.2*yp + 0.6*yp2 + 0.2*yp3
    yp = yp2
    scores += list(yp)


scores = np.array(scores)

boolscores = scores.copy()
boolscores[boolscores<0.5] = 0
boolscores = boolscores.astype(np.bool)
print(f"Found {sum(boolscores)} positives in {len(boolscores)} entries.")


#%% 
import pandas as pd
from os.path import basename 
ras, decs = [], []
for f in all_gri:
    f = basename(f)
    split = basename(f).split('_')
    ra, dec = float(split[0]), float(split[1].replace('.npy', ''))
    ras.append(ra)
    decs.append(dec)

# %%
df = {'RA':ras[:len(scores)], 'DEC':decs[:len(scores)], 'filename':all_gri[:len(scores)], 'score':scores}
df = pd.DataFrame(df)
df.to_csv('scores_3rd_part_miliquas_training_only_on_nice.csv')