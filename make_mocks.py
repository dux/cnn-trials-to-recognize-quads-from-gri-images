#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from scipy import interpolate
from os.path import join 
from imageSim import SBModels,SBObjects,convolve
import indexTricks as iT



plot = 0
#this is the quasar template from Van den Someone... 2001?
newspec = np.loadtxt('quasar_template_vdb.txt')

wav = newspec.T[0]
flux = newspec.T[1]

wavlower = np.arange(0.5, 800.5, 1)
fluxlower = np.zeros(wavlower.shape)

medhigher = np.median(flux[np.where((wav>7000)&(wav<8000))[0]])
wavupper = np.arange(8555.5, 9000.5, 1)
fluxupper = medhigher*np.ones(wavupper.shape)

quasarwav = np.array(list(wavlower)+list(wav)+list(wavupper))
quasarflux = np.array(list(fluxlower)+list(flux)+list(fluxupper))


#galaxy template is from Kinney et al. (1996) https://archive.stsci.edu/hlsps/reference-atlases/cdbs/grid/kc96/ 
elliptical = fits.open('./elliptical_template.fits')[1].data
wav = elliptical['WAVELENGTH']
flux = elliptical['FLUX']
wavlower = np.arange(0., 2005., 5.)
k = np.where(wav>2000.)

galaxywav = np.array(list(wavlower)+list(wav[k]))
galaxyflux = np.array(list(flux.min()*np.ones(wavlower.shape))+list(flux[k]))


sloang = np.loadtxt('./filters/sloan_g.txt')
sloanr = np.loadtxt('./filters/sloan_r.txt')
sloani = np.loadtxt('./filters/sloan_i.txt')

sloang_lambdas = sloang.T[0]*10.
sloang_flux = sloang.T[1]
keepg = np.where((sloang_lambdas>3790)&(sloang_lambdas<6000))[0]
sloang_lambdas = sloang_lambdas[keepg]
sloang_flux = sloang_flux[keepg]

sloanr_lambdas = sloanr.T[0]*10.
sloanr_flux = sloanr.T[1]
keepr = np.where((sloanr_lambdas>5250)&(sloanr_lambdas<7500))[0]
sloanr_lambdas = sloanr_lambdas[keepr]
sloanr_flux = sloanr_flux[keepr]

sloani_lambdas = sloani.T[0]*10.
sloani_flux = sloani.T[1]
keepi = np.where((sloani_lambdas>6700)&(sloani_lambdas<8700))[0]
sloani_lambdas = sloani_lambdas[keepi]
sloani_flux = sloani_flux[keepi]

bgnoises = {'g':0.108, 'r':0.165, 'i':0.152} #these should be drawn from realistic Pan-STARRS images in association with zeropoints, 
# they are DES-like for now
zp=25 #zeropoint
#%%
def K_correction(zobs, band='r'):
    #interpolate the quasarflux onto the filter wavelengths
    mod = interpolate.splrep(quasarwav*(1+zobs), quasarflux)
    if band=='g':
        quasar_g = interpolate.splev(sloang_lambdas, mod)
        weightedflux = np.sum(quasar_g*sloang_flux)/np.sum(sloang_flux)/(1+zobs)

    if band=='r':
        quasar_r = interpolate.splev(sloanr_lambdas, mod)
        weightedflux = np.sum(quasar_r*sloanr_flux)/np.sum(sloanr_flux)/(1+zobs)

    if band=='i':
        quasar_i = interpolate.splev(sloani_lambdas, mod)
        weightedflux = np.sum(quasar_i*sloani_flux)/np.sum(sloani_flux)/(1+zobs)

    #K correction to z=0 i-band
    z=0
    quasar_i = interpolate.splev(sloani_lambdas, mod)
    weightedfluxi = np.sum(quasar_i*sloani_flux)/np.sum(sloani_flux)/(1+z)

    Kcorrection = -2.5*np.log10(weightedflux/weightedfluxi)
    return Kcorrection


def K_correction_galaxy(zobs, band='r'):
    #interpolate the quasarflux onto the filter wavelengths
    mod = interpolate.splrep(galaxywav*(1+zobs), galaxyflux)
    if band=='g':
        galaxy_g = interpolate.splev(sloang_lambdas, mod)
        weightedflux = np.sum(galaxy_g*sloang_flux)/np.sum(sloang_flux)/(1+zobs)

    if band=='r':
        galaxy_r = interpolate.splev(sloanr_lambdas, mod)
        weightedflux = np.sum(galaxy_r*sloanr_flux)/np.sum(sloanr_flux)/(1+zobs)

    if band=='i':
        galaxy_i = interpolate.splev(sloani_lambdas, mod)
        weightedflux = np.sum(galaxy_i*sloani_flux)/np.sum(sloani_flux)/(1+zobs)

    #K correction to z=0 i-band
    z=0
    galaxy_i = interpolate.splev(sloani_lambdas, mod)
    weightedfluxi = np.sum(galaxy_i*sloani_flux)/np.sum(sloani_flux)/(1+z)

    Kcorrection = -2.5*np.log10(weightedflux/weightedfluxi)
    return Kcorrection


#%%
mocks = fits.open('./qso_mock.fits')[1].data
N_images = mocks['NIMG']
IMSEP = mocks['IMSEP']

#ONLY KEEP QUADS AND THOSE WITH LARGE  ENOUGH SEPARATION
lenses = mocks[np.where((N_images==4)&(IMSEP>1.5))]

lenses = lenses[np.where(lenses['ZSRC']<3.7)]
from os.path import basename
from glob import glob

# instead of filtering more (see all the commented code below)
# I selected manually the indexes of all the good looking
# mocks configurations. We take those only:
where = np.load("manually_selected_indexes_mocks.npy")
lenses = lenses[(where, )]




# now only keep lenses with at least 3 well visible images
# mags_cat    = lenses['MAGI_IN'][:,np.newaxis] - 2.5*np.log10(abs(lenses['MAG']))
# sorted_mags = np.sort(mags_cat, axis=-1)
# where the 3rd brightest image has magnitude below 22:
# lenses = lenses[np.where(sorted_mags[:,2]>=22)]


# now remove the lenses where the flux ratio between the two furthest images is too big.
# good = []
# for i in range(len(lenses)):
#     xs, ys = lenses['XIMG'][i], lenses['YIMG'][i]
#     distmax = 0
#     furthest = (0,1)
#     for c in range(len(xs)):
#         for k in range(len(xs)):
#             dist = (xs[c]-xs[k])**2 + (ys[c]-ys[k])**2
#             if dist > distmax:
#                 furthest = (c, k)
#                 distmax = dist
#     print(furthest)
#     fluxratio = ( 10**(-mags_cat[i][furthest[0]]) ) / ( 10**(-mags_cat[i][furthest[1]]) )
#     # fluxratio = ( 10**(-min(mags_cat[i])) ) / ( 10**(-max(mags_cat[i])) )
#     # print(fluxratio)
#     if fluxratio > 0.2 and fluxratio < 5:
#         good.append(i)
# lenses = lenses[(np.array(good), )]
        
#%%
mags_cat    = lenses['MAGI_IN'][:,np.newaxis] - 2.5*np.log10(abs(lenses['MAG']))
sorted_mags = np.sort(mags_cat, axis=-1)
diffs = sorted_mags[:,2]-22
diffs = diffs + np.random.uniform(0, 0.2, size=diffs.size)
#%%
scale = 0.25
imsize = 50    # Simulated image size

# Transform variables to axis ratio, North of West(right)
q = 1.-lenses['ELLIP']
pa = lenses['PHIE']+90.




# galmags = lenses['APMAG_I']

# # one more filter, must have that the galaxy isn't much brighter than the lens.
# mags_cat = lenses['MAGI_IN'][:,np.newaxis] - 2.5*np.log10(abs(lenses['MAG']))
# mags_av  = np.mean(mags_cat, axis=1)
# galnottoobright = np.where(mags_av - galmags < 4)
# lenses = lenses[galnottoobright]
# diffs = diffs[galnottoobright]

galmags = lenses['APMAG_I']


# Convert to V-band luminosity (hence the 0.654 color term)
gallum_v = 0.4*(4.8-(0.654+lenses['ABMAG_I']))-9. #convert to V-band because SLACS uses this
sigma = lenses['VELDISP']

# Use the SLACS Fundamental Plane to go from lum+sigma to size, see Auger(?) paper
a = 1.020
b = -0.872
c = -0.108
reff = (a*np.log10(sigma/100.)+b*gallum_v-b*np.log10(2*np.pi)+c)/(1.+2*b)

# Get Sersic index from e.g., Caon93ls
n = 10**(0.34+0.22*reff)

# Convert to arcsec
re = 10**reff/(1000*lenses['DD']/206265.)
save_dir = "/media/frederic/kingston_data/train_synthetic_quads/synthetic_positives_filtered/"
for iiii in range(lenses.size):
    colorimg = []
    allgalmags = []
    for randomxtranslation in np.random.uniform(-5, 5, 4):
        for randomytranslation in np.random.uniform(-5, 5, 4):
            for theta in np.random.uniform(0, 180, 4):
                colorimg = []
                allgalmags = []
                fwhm = np.random.uniform(3.5, 4.5)
                xi = lenses['XIMG'][iiii].copy()
                yi = lenses['YIMG'][iiii].copy()
                xi = xi + np.random.normal(0., 0.1, 4)
                yi = yi + np.random.normal(0., 0.1, 4)
                for band in ['i', 'r', 'g']:
                    # Define coordinates
                    y,x = iT.coords((imsize,imsize))
                    x -= x.mean() 
                    y -= y.mean()
                    
                    x += randomxtranslation
                    y += randomytranslation
                    x *= scale
                    y *= scale
            
                    mags = lenses['MAGI_IN'][iiii] - 2.5*np.log10(abs(lenses['MAG'][iiii]))
                    mags -= diffs[iiii]
            
                    imaggal = galmags[iiii]
                    Kcorr   = K_correction_galaxy(zobs=lenses['ZLENS'][iiii], band=band) - \
                              K_correction_galaxy(zobs=lenses['ZLENS'][iiii], band='i')
                    print(Kcorr)
                    galmag = imaggal + Kcorr
                    allgalmags.append(galmag)
            
                    
                    # First evaluate the lensing galaxy light
                    gal = SBObjects.Sersic('',{'x':0.,'y':0.,'q':q[iiii],'pa':pa[iiii]+theta,'re':re[iiii],'n':n[iiii]})
                    gal.setAmpFromMag(galmag, zp)
                    img = gal.pixeval(x, y, scale=scale)
            
                    PSF = SBObjects.Moffat('', {'x': 0., 'y': 0., 'fwhm': fwhm, 'q': 1., 'pa': 0., 'index': 3.75})
                    yy, xx = np.indices((17, 17)).astype(np.float32)
                    xx -= xx.mean()
                    yy -= yy.mean()
                    #PSF.setPars()
                    P = PSF.pixeval(xx, yy)
                    P /= P.sum()
                    # Convolve extended-object image
                    img = convolve.convolve(img, P)[0]
                    index_mag=0
                    # Finally add the AGNs
                    
                    
                    #since the K correction is relative to the i-band
                    Kcorr = K_correction(zobs=lenses['ZSRC'][iiii], band=band) -\
                            K_correction(zobs=lenses['ZSRC'][iiii], band='i')

                    for j in range(xi.size):
                        thetarad = theta/180*np.pi 
                        xi_ = np.cos(thetarad)*xi[j] - np.sin(thetarad)*yi[j] 
                        yi_ = np.sin(thetarad)*xi[j] + np.cos(thetarad)*yi[j] 
                        qso = SBObjects.Moffat('', {'fwhm':fwhm,'index':3.75,'pa':0.,'q':1.,'x':xi_/scale,'y':yi_/scale})
                        color = qso.Mag(zp)-mags[j] - Kcorr + np.random.normal(0, 0.1)
                        print('color:', color)
                        qso.amp = 10**(0.4*color)
                        img += qso.pixeval(x/scale,y/scale)
                        index_mag=j
            
                    colorimg.append( img + np.random.normal(0, bgnoises[band]+np.random.uniform(-0.02,0.02), img.shape) )
                
                colorimg = np.array(colorimg)
                colorimg = np.moveaxis(colorimg, 0, 2)
                galmagstring = f"{allgalmags[2]:.03f}_{allgalmags[1]:.03f}_{allgalmags[0]:.03f}"
                magstring = '_'.join([f"{mags[i]:.03f}" for i in range(4)])
                savename =f"zsource:{lenses['ZSRC'][iiii]:.02f}_zlens:{lenses['ZLENS'][iiii]:.02f}_"
                savename+=f"mags:{magstring}_imsep:{lenses['IMSEP'][iiii]:.02f}_lens-i:{galmags[iiii]:.03f}_galmags:{galmagstring}_"
                savename+=f"dx:{randomxtranslation:.02f}_dy:{randomytranslation:.03f}_dtheta:{theta:.01f}.npy"
                np.save(join(save_dir, savename), colorimg)
                
                if plot:
                    plt.switch_backend('Agg')
                    scaled_cimg = colorimg-np.percentile(colorimg, 0.5)
                    scaled_cimg = scaled_cimg / np.percentile(scaled_cimg, 99.5)
                    plt.figure(figsize=(8., 8.))
                    plt.imshow(scaled_cimg, interpolation='nearest', origin='lower')
                    galmagstring = str(allgalmags[2])[:5] + ' ' + str(allgalmags[1])[:5] + ' ' + str(allgalmags[0])[:5]
                    magstring = str(mags[0])[:5] + ' ' + str(mags[1])[:5] + ' ' + str(mags[2])[:5] + ' ' + str(mags[3])[:5]
                    plt.title('zsource:'+str(lenses['ZSRC'][iiii])+' zlens:'+str(lenses['ZLENS'][iiii])+' \n mags:'+magstring +' \n imsep:'+str(lenses['IMSEP'][iiii])[:4]+'" lens_i:'+str(galmags[iiii])[:5]+' \n galmags (g, r, i):'+str(galmagstring))
                    # savename = 
                    # plt.scatter(img.shape[0]/2.+yi/scale-0.5, img.shape[0]/2.+xi/scale-0.5, s=5)
                    # plt.waitforbuttonpress()
                    plt.savefig(join(save_dir, f"{iiii}_{theta:.02f}.jpg"))
                    # plt.show(block=True)

