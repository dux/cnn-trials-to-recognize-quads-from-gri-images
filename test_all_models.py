#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 23:07:56 2020

@author: frederic
"""


from os.path import join, basename 
from glob import glob 
import numpy as np
N = 7
from sklearn.metrics import confusion_matrix 
from keras.models import load_model 

def testMetric(models):
    d = '/media/frederic/kingston_data/train_synthetic_quads/test/*npy'
    d = glob(d)
    y = np.array([int(basename(e).split('_')[0]) for e in d])
    arrays = []
    for e in d:
        im = np.load(e)[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        arrays.append(im.astype(np.float32))
    
    
    for model in models:
        mod = load_model(model)
        yp = mod.predict(np.array(arrays)).flatten()
        yp[yp<0.5] = 0
        yp = yp.astype(bool)
        c = confusion_matrix(y, yp)
        print("============================")
        print(model)
        print(c)


d1 = 'high_false_positive_training'
d2 = 'intense_training'
s= 'random_stuff_mocks'
# models = glob(join(s, '*test2*.model'))
# models.sort()
# models = glob(join(d1, '*.model')) + glob(join(d2, '*.model')) + glob('*.model')
models = ['model_only_nice_positives_at_i=0.model']
testMetric(models)