#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 22:10:42 2020

@author: frederic
"""
import pandas as pd

csv1 = "scores_1st_part_miliquas_training_only_on_nice.csv"
csv2 = "scores_2nd_part_miliquas_training_only_on_nice.csv"
csv3 = "scores_3rd_part_miliquas_training_only_on_nice.csv"

df1, df2, df3 = pd.read_csv(csv1), pd.read_csv(csv2), pd.read_csv(csv3)

df = pd.concat([df1,df2,df3])

df.drop_duplicates(subset=['RA', 'DEC'], inplace=True, keep='first')

goodones = df[df['score']>0.5]

goodones.to_csv('miliquas_training_only_on_nice__all_positives.csv')