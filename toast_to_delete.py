#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 16:19:29 2020

@author: frederic
"""


M_phosgene = 1.4
M_toluene = 8.5

mass_phosgene = 98.92
mass_toluene = 92.14

# mass_total = M_toluene*mass_toluene + M_phosgene*mass_phosgene

x_phosgene = M_phosgene / ( M_phosgene + M_toluene )
x_toluene = M_toluene / ( M_phosgene + M_toluene )

def partialPressureToluene(T):
    A = 4.23679
    B = 1426.448
    C = -45.957
    return 10**(A - B / (T + C))


def partialPressurePhosgene(T):
    A = 4.27105
    B = 1072.71
    C = -29.855
    return 10**(A - B / (T + C))


def totalPressure(T, x_phosgene, x_toluene):
    p_phosgene = partialPressurePhosgene(T)*x_phosgene
    p_toluene  = partialPressureToluene(T)*x_toluene
    return p_phosgene + p_toluene 