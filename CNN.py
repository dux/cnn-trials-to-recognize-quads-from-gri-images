#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 18:28:29 2020

@author: frederic
"""


import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K

from glob import glob
from os.path import join
import numpy as np

# number of stdev before clipping: (normalisation of the training)
N = 7



img_rows, img_cols, nchannels = 40, 40, 3
input_shape = (img_rows, img_cols, nchannels)
# """

model = Sequential()
model.add(Conv2D(32, (7, 7), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(64, (5, 5), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(128, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(1024, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(512,  activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(32,   activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(1,    activation='sigmoid'))

optimizer = keras.optimizers.SGD(learning_rate=0.01)

model.compile(loss=keras.losses.binary_crossentropy,
              optimizer=optimizer,#optimizer,#keras.optimizers.Adadelta(),
              metrics=['accuracy'])
# """
# model = keras.models.load_model('trial_on_24000')

#%%
negdir = '/media/frederic/kingston_data/train_synthetic_quads/real_negatives_extended'
negdir2 = '/media/frederic/kingston_data/train_synthetic_quads/real_multiplets'
miliquasdir = '/media/frederic/kingston_data/train_synthetic_quads/miliquas_goal'
posdir = '/media/frederic/kingston_data/train_synthetic_quads/synthetic_positives_filtered'
lfns = glob(join(negdir, '0_gri_arrays', '*npy'))
lfnm = glob(join(negdir2, '0_gri_arrays', '*npy'))
lfnq = glob(join(miliquasdir, '0_gri_arrays', '*npy'))
lfp  = glob(join(posdir, '*npy'))
lfk  = glob("/media/frederic/kingston_data/train_synthetic_quads/known_quads/0_gri_arrays/*")

np.random.shuffle(lfns)
np.random.shuffle(lfnm)
np.random.shuffle(lfnq)
np.random.shuffle(lfp)


# now, unfortunately we will need a lot more of negatives to be able to go
# through all the positives:
lfnm = 5*lfnm 
lfns = 4*lfns 
lfnq = 2*lfnq


#%%
def reload(lim):
    negs = []
    for e in lfns[lim:lim+3000]:
        im = np.load(e)[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        negs.append(im.astype(np.float32))
    
    for e in lfnm[lim:lim+3000]:
        im = np.load(e)[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        negs.append(im.astype(np.float32))
    
    
    for e in lfnq[lim:lim+17008-6000]:
        im = np.load(e)[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        negs.append(im.astype(np.float32))
        
    poss = []
    for e in lfp[lim*3:lim*3+8000*3]:
        im = np.load(e)[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        poss.append(im.astype(np.float32))

    negs = np.array(negs)
    negs[np.where(np.isnan(negs))] = 0.1 
    poss = np.array(poss)
    
    x_train = np.concatenate((negs, poss))
    y_train = np.concatenate((np.zeros(negs.shape[0]), np.ones(poss.shape[0])))
    return lim+8000, x_train, y_train
    
def test_model(model, lim):
    knowns = []
    for e in lfk:
        im = np.load(e)[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        knowns.append(im.astype(np.float32))
    knowns = np.array(knowns) 
    knowns[np.where(np.isnan(knowns))] = 0.1 
    
    ypk = model.predict(knowns)
    ypk = ypk.flatten()
    filename = f'test_on_real_at_lim_{lim}'
    np.savetxt(filename, ypk)
    ypk[ypk<0.5] = 0
    ypk = ypk.astype(bool)
    print(f"{sum(ypk)/len(ypk):.01%} of known lenses found.")


#%%
lim = 0
for i in range(3):
    lim, x_train, y_train = reload(lim)
    model.fit(x=x_train, y=y_train, epochs=4, validation_split=0.1)
    test_model(model, lim)
    model.save(f"model_only_nice_positives_at_i={i}.model")
    


#%%
# """
negs = []
np.random.shuffle(lfnm)
for e in lfnm[:1000]:
    im = np.load(e)[5:-5, 5:-5, :]
    m, s = np.nanmean(im), np.nanstd(im)
    im[im>m+N*s] = m+N*s
    im -= np.nanmin(im)
    im /= np.nanmax(im)
    negs.append(im.astype(np.float32))

# poss = []
# lfp = glob(join(posdir, '*npy'))
# np.random.shuffle(lfp)
# for e in lfp[:1000]:
#     im = np.load(e)
#     m, s = np.nanmean(im), np.nanstd(im)
#     im[im>m+N*s] = m+N*s
#     im -= np.nanmin(im)
#     im /= np.nanmax(im)
#     poss.append(im.astype(np.float32))


negs = np.array(negs)
negs[np.where(np.isnan(negs))] = 0.1 
# poss = np.array(poss)

x_test = negs#np.concatenate((negs, poss))
y_test = np.zeros(len(negs))
# y_test = np.concatenate((np.zeros(negs.shape[0]), np.ones(poss.shape[0])))


yp = model.predict(x_test)
yp = yp.flatten()
yp[yp<0.5] = 0
yp[yp>=0.5] = 1
from sklearn.metrics import confusion_matrix

print(confusion_matrix(y_test, yp))
# """

#%%


