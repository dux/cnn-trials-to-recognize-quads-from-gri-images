#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 22:24:10 2020

@author: frederic
"""
from glob import glob
from os.path import join
import numpy as np
tests = [
 'test_on_real_at_lim_64000',
 'test_on_real_at_lim_72000',
 'test_on_real_at_lim_80000',
 'test_on_real_at_lim_88000',
 'test_on_real_at_lim_96000',
 'test_on_real_at_lim_104000',
 'test_on_real_at_lim_112000',
 'test_on_real_at_lim_120000',
 'test_on_real_at_lim_128000',
 'test_on_real_at_lim_136000',
 'test_on_real_at_lim_144000',
 'test_on_real_at_lim_152000',
 'test_on_real_at_lim_160000',
 'test_on_real_at_lim_168000',
 'test_on_real_at_lim_176000',
 'test_on_real_at_lim_184000',
 'test_on_real_at_lim_192000',
 'test_on_real_at_lim_200000',
 'test_on_real_at_lim_208000',
 'test_on_real_at_lim_216000',
 'test_on_real_at_lim_224000',
 'test_on_real_at_lim_232000',
 'test_on_real_at_lim_240000',
 'test_on_real_at_lim_248000',
 'test_on_real_at_lim_256000',
 'test_on_real_at_lim_264000']

lfk  = glob("/media/frederic/kingston_data/train_synthetic_quads/known_quads/0_gri_arrays/*")
N = 7
import matplotlib.pyplot as plt
plt.switch_backend('agg')
for e in tests:
    num = float(e.split('_')[-1])/264000
    print(num, e)
#%%
    yp = np.loadtxt(e) 
    fig, axs = plt.subplots(7, 6, figsize=(8,11))
    axs = axs.flatten()
    plt.suptitle(f'training progress: {num:.0%}; {sum(yp)/len(yp):.0%} recognized.')
    for y, e, ax in zip(yp, lfk, axs):
        im = np.load(e)[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        ax.imshow(im, interpolation='nearest', origin='lower')
        # ax.set_aspect('auto')
        ax.text(x=0.1, y=0.1, s=f"{y:.02f}", color='white')
    for ax in axs:
        ax.set_xticks([])
        ax.set_yticks([])
    plt.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.savefig(f"results/{num:.02f}.jpg", dpi=250)

