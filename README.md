# mocks
Code to create mock lensed quasar images based on OM10.

Creates g, r, i colour images of lensed quasars based on the Oguri and Marshall 2010 catalogue. 

Contact: cameron.lemon@epfl.ch

Requirements: numpy, scipy, astropy

# CNN
for questions: frederic.dux@epfl.ch
- Use the file `make_mocks.py` to create mock images of lensed quasars. 
- Using the mocks and real data, train a CNN with `CNN.py`. The resulting best model I could make is `model_only_nice_positives_at_i=0.model`.
- Then, sort gri images using `run_cnn_model_on_miliquas_catalogue.py`. This will output a csv file containing the results.
You will unfortunately have to adapt the ins and outs of the said scripts. 

The main thing is the resulting model, and using keras to load it and assign scores to images. The said images must be of shape (40,40,3) and be normalized between 0 and 1 (with a 7 sigma clipping, see where the data is loaded in `CNN.py`. 