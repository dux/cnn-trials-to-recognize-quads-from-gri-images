#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 10:18:24 2020

@author: frederic
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

N = 12
def loadimage(gripath):
    im = np.load(gripath)[5:-5, 5:-5, :]
    m, s = np.nanmean(im), np.nanstd(im)
    im[im>m+N*s] = m+N*s
    im -= np.nanmin(im)
    im /= np.nanmax(im)
    return im

# df = pd.read_csv('scores_2nd_part_miliquas_training_high_density_adversarial.csv')
df = pd.read_csv('scores_1st_part_miliquas_training_only_on_nice.csv')

dfp = df[df['score']>0.5]

filenames = dfp['filename']
#%%
gris = []
for f in filenames:
    gris.append(loadimage(f))
leng = len(gris)
gris = np.array(gris)
gris[np.where(np.isnan(gris))] = 0.1
#%%
# model2 = load_model('model_at_i=32.model')
# yp2 = model2.predict(gris).flatten()

# where2 = np.where(yp2>0.9)

#%%

# i = 0
j = 0
while 1:
    fig, axs = plt.subplots(7, 6, figsize=(8,11))
    axs = axs.flatten()
    for ax in axs:
        # i = np.random.randint(0, leng)
        # j = where2[0][i]
        im = gris[j]
        ax.imshow(im, interpolation='nearest', origin='lower')
        # ax.text(x=0.1, y=0.1, s=f"j={j} -- {float(yp2[j]):.02f}", color='white')
        ax.text(x=0.1, y=0.1, s=f"i={j} -- {float(dfp[['score']].iloc[j]):.02f}", color='white')
        j+=1
    for ax in axs:
        ax.set_xticks([])
        ax.set_yticks([])
    plt.tight_layout()
    plt.waitforbuttonpress()
