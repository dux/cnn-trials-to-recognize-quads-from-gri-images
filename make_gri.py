#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 23 18:38:25 2020

@author: frederic
"""
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
basename = "83.6332_22.0145"

fluxes = []
for c in 'gri':
    f = basename+f'_{c}.fits'
    e = f.replace('.fits', '.exp.fits')
    f = fits.open(f)[0]
    gain = f.header['HIERARCH CELL.GAIN']
    e = fits.getdata(e)
    f = f.data*gain/e
    fluxes.append(f)
    
fluxes = np.array(fluxes).T 

plt.imshow(fluxes/np.max(fluxes), interpolation='nearest', origin='lower')