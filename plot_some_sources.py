#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 09:15:28 2020

@author: frederic
"""



from glob import glob
import matplotlib.pyplot as plt
from os.path import join

from random import choice
import numpy as np


# npydirectory = "/media/frederic/kingston_data/train_synthetic_quads/real_negatives_extended/0_gri_arrays"
# npydirectory2 = "/media/frederic/kingston_data/train_synthetic_quads/real_multiplets/0_gri_arrays"
# npydirectory3 = "/media/frederic/kingston_data/train_synthetic_quads/miliquas_goal/0_gri_arrays"
# npydirectory4 = "/media/frederic/kingston_data/train_synthetic_quads/synthetic_positives/"
# files = glob(join(npydirectory4, '*npy'))
# files = lfp

# files = glob(join(npydirectory, '*.npy'))
# # files+= glob(join(npydirectory2, '*.npy'))
# files+= glob(join(npydirectory3, '*.npy'))
# filesp= glob(join(npydirectory4, '*.npy'))


# dirs = '/media/frederic/kingston_data/train_synthetic_quads/real_multiplets/0_gri_arrays'
# dirpos = join(dirs, 'poss')
# dirneg = join(dirs, 'negs')
# files = glob(join(dirs, '*npy'))
# filesp= glob(join(dirpos, '*npy'))


directory = '/media/frederic/kingston_data/train_synthetic_quads/synthetic_positives_filtered/*npy'
files = glob(directory)
#%%
fig, axs = plt.subplots(5, 5, figsize=(10,10))
axs = axs.flatten()
from os.path import basename
N = 7
while 1:
    for i in range(25):
        cho = choice(files)
        print(cho)
        im = np.load(cho)[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        axs[i].imshow(im, interpolation='nearest', origin='lower')
        axs[i].set_xticks([])
        axs[i].set_yticks([])
        # axs[i].set_title(basename(cho))
    # for i in range(15, 25):
    #     im = np.load(choice(filesp))[5:-5, 5:-5, :]
    #     m, s = np.nanmean(im), np.nanstd(im)
    #     im[im>m+N*s] = m+N*s
    #     im -= np.nanmin(im)
    #     im /= np.nanmax(im)
    #     axs[i].imshow(im, interpolation='nearest', origin='lower')
    #     axs[i].set_xticks([])
    #     axs[i].set_yticks([])
    plt.tight_layout()
    plt.waitforbuttonpress()