#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 21:49:08 2020

@author: frederic
"""


import numpy as np
import pandas as pd
from os.path import join, exists
from os import makedirs
import multiprocessing
from shutil import rmtree
from download_gri import build_image



prevdir1 = "/media/frederic/kingston_data/train_synthetic_quads/miliquas_goal"
prevdir2 = "/media/frederic/kingston_data/train_synthetic_quads/miliquas_goal2"

maindirectory = "/media/frederic/kingston_data/train_synthetic_quads/miliquas_goal3"
complete_images = join(maindirectory, '0_gri_arrays')
makedirs(maindirectory, exist_ok=1)
makedirs(complete_images, exist_ok=1)
radec = pd.read_csv('miliquas_recno_coords.csv')
start, end = 0, len(radec)




# dispatched between several computers:

#%%    
def actual_download(entry):
    try:
        i, recno, ra, dec = entry
        filename = f"{ra:.04f}_{dec:.04f}.npy"
        if exists(join(prevdir1, '0_gri_arrays', filename)) \
            or exists(join(prevdir2, '0_gri_arrays', filename))\
            or exists(join(complete_images, filename)):
            return
        outdir = join(maindirectory, f"{i}_{ra:.04f}_{dec:.04f}")
        print(f"Downloading object no {i} (recno {recno}) at {outdir}")
        image = build_image(ra, dec, hsize=50, outdir=outdir)
        total_file = join(complete_images, filename)
        np.save(total_file, image)
        del image
        rmtree(outdir)
    except:
        print(f'bad stuff with {i}')
    return
#%%
entries = [(i, radec['recno'][i], radec['_RAJ2000'][i],  radec['_DEJ2000'][i]) for i in range(start, end)]

#%%
pool = multiprocessing.Pool(20, maxtasksperchild=1000)
pool.map(actual_download, entries)



