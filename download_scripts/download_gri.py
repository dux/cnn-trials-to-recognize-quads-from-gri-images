#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 23 18:00:21 2020

@author: frederic
"""
import  urllib3
import  wget
from    os.path                    import  join, exists
from    os                         import  makedirs

import  numpy as np
from    astropy.io                 import  fits


def download_GRI_fits(ra, dec, hsize=50, outdir='.'):
    """

    Parameters
    ----------
    ra, dec: floats
          J2000 coordinates
    hsize : int, optional
        size of the gri image to download. The default is 50.
    outdir : str, optional
        working directory. The default is '.'.

    Raises
    ------
    IndexError
        If there is not enough data to build a gri image at the (ra,dec) location.

    Returns
    -------
    int
        1 if success
    """
    if not exists(outdir):
        makedirs(outdir)
    
    alreadydone = True 
    fnames = []
    for channel in 'gri':
        imgname = join(outdir, f"{ra:.04f}_{dec:.04f}_{channel}.fits")
        if exists(imgname):
            fnames.append(imgname)
        else:
            alreadydone = False
    if alreadydone:
        return fnames
    service = "http://ps1images.stsci.edu/cgi-bin/ps1filenames.py"
    url = ("{service}?ra={ra}&dec={dec}&size={hsize}&format=fits"
           "&filters=gri").format(**locals())
    http    = urllib3.PoolManager()
    files   = http.request('GET', url, preload_content=0).data.decode('utf-8').split('\n')
    files   = [f.split(' ')[-3] for f in files[1:] if f]
    if len(files)<3:
        raise IndexError
    url = ("https://ps1images.stsci.edu/cgi-bin/fitscut.cgi?"
           "ra={ra}&dec={dec}&size={hsize}&format=fits").format(**locals())
    fnames, expnames = [], []
    for f in files:
        channel = f.split('.')[-3]
        
        imagename = join(outdir, f"{ra:.04f}_{dec:.04f}_{channel}.fits")
        wget.download(url+'&red='+f, out=imagename)
        
        
        
        #### apparently, no need to download the exp time maps.
        #### the 'exptime' header entry already contains this information,
        #### moreover the images were already corrected so that they look homogenous. 
        #### --> better to use the "average" exposure time than the map.
        
        # expname = imagename.replace('.fits', '.exp.fits')
        # wget.download(url+'&red='+f.replace('.fits', '.exp.fits'), out=expname)
    return 1



def build_image(ra, dec, hsize, outdir):
    """

    Parameters
    ----------
    ra, dec: floats
          J2000 coordinates
    hsize : int, optional
        size of the gri image to download. The default is 50.
    outdir : str, optional
        working directory. The default is '.'.

    Returns
    -------
    fluxes : numpy array
        numpy array of floats, shape (hsize, hsize, 3), normalized between 0 and 1.
        
    """
    download_GRI_fits(ra, dec, hsize=hsize, outdir=outdir)
    basename = f"{ra:.04f}_{dec:.04f}"
    fluxes = []
    for c in 'irg':
        f = join(outdir, basename + f'_{c}.fits')
        
        #### same as above, useless to have exposure time maps. --> commented out
        # e = f.replace('.fits', '.exp.fits')
        # e = fits.getdata(e)
        
        
        f = fits.open(f)[0]
        gain = f.header['HIERARCH CELL.GAIN']
        exptime = f.header['exptime']
        
        f = f.data*gain/exptime
        fluxes.append(f)
        
    fluxes = np.array(fluxes)
    fluxes = np.moveaxis(fluxes, 0, 2)
    fluxes = (fluxes-np.min(fluxes))
    fluxes = fluxes/np.max(fluxes)
    return fluxes


if __name__ == "__main__":
    ra, dec = 69.5619, -12.28745
    ra, dec = 316.9685, -16.1921
    # ra, dec = 197.5835, -17.24939
    # ra, dec = 234.355598, -30.171335
    # ra, dec = 	33.568125, -21.093072
    # ra, dec = 26.79231, 46.5118
    fluxes = build_image(ra, dec, 50, 'test')
    m, s = np.nanmean(fluxes), np.nanstd(fluxes)
    N = 7
    fluxes[fluxes>m+N*s] = m+N*s
    fluxes -= np.nanmin(fluxes)
    fluxes /= np.nanmax(fluxes)
    import  matplotlib.pyplot  as  plt
    plt.figure(figsize=(8., 8.))
    plt.imshow(fluxes, interpolation='nearest', origin='lower')
    plt.show()
    fluxes = fluxes.reshape((1,50,50,3))
