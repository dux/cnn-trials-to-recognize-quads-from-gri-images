#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 21:49:08 2020

@author: frederic
"""


import numpy as np
from os.path import join
from os import makedirs
import multiprocessing
from download_gri import build_image





maindirectory = "/mnt/FE867C3B867BF28F/train_cnn_recognize_quads/real_negatives_extended"
complete_images = join(maindirectory, '0_gri_arrays')
makedirs(maindirectory, exist_ok=1)
makedirs(complete_images, exist_ok=1)
coords = np.loadtxt('extended_sources.txt')
start, end = 0, 25000



RA, DEC = coords[0], coords[1]

# dispatched between several computers:

    
def actual_download(entry):
    i, ra, dec = entry
    outdir = join(maindirectory, f"i_{ra:.04f}_{dec:.04f}")
    print(f"Downloading object no {i} at {outdir}")
    image = build_image(ra, dec, hsize=50, outdir=outdir)
    total_file = join(complete_images, f"{ra:.04f}_{dec:.04f}.npy")
    np.save(total_file, image)

entries = [(i, RA[i], DEC[i]) for i in range(start, end)]


pool = multiprocessing.Pool(10)
pool.map(actual_download, entries)



