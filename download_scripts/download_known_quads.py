#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 21:49:08 2020

@author: frederic
"""


import numpy as np
import pandas as pd
from os.path import join
from os import makedirs
import multiprocessing
from shutil import rmtree
from astropy.io import fits
from download_gri import build_image





maindirectory = "/media/frederic/kingston_data/train_synthetic_quads/known_quads"
complete_images = join(maindirectory, '0_gri_arrays')
makedirs(maindirectory, exist_ok=1)
makedirs(complete_images, exist_ok=1)
radec = fits.getdata('quads.fits')
radec = radec[np.where(radec['DEC']>-30)]
seps = np.array([float(e) for e in radec['separation']])
radec = radec[seps>1.35]
start, end = 0, len(radec)
#%%


# dispatched between several computers:

    
def actual_download(entry):
    name, ra, dec = entry
    outdir = join(maindirectory, f"{name}_{ra:.04f}_{dec:.04f}")
    print(f"Downloading object {name} at {outdir}")
    image = build_image(ra, dec, hsize=50, outdir=outdir)
    total_file = join(complete_images, f"{ra:.04f}_{dec:.04f}.npy")
    np.save(total_file, image)
    del image
    rmtree(outdir)

entries = [(radec['name'][i], radec['RA'][i],  radec['DEC'][i]) for i in range(start, end)]


pool = multiprocessing.Pool(10)
pool.map(actual_download, entries)



