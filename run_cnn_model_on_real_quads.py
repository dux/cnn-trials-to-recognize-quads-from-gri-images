#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

"""
from glob import glob
from os.path import join
import numpy as np

from keras.models import load_model 

model = 'model_only_nice_positives_at_i=0.model'
model = load_model(model)

lfk  = glob("/media/frederic/kingston_data/train_synthetic_quads/known_quads/0_gri_arrays/*")
N = 7
import matplotlib.pyplot as plt
#%%
fig, axs = plt.subplots(7, 6, figsize=(8,11))
axs = axs.flatten()
for e, ax in zip(lfk, axs):
    im = np.load(e)[5:-5, 5:-5, :]
    m, s = np.nanmean(im), np.nanstd(im)
    im[im>m+N*s] = m+N*s
    im -= np.nanmin(im)
    im /= np.nanmax(im)
    im = im[:,::-1,:]
    y = float(model.predict(im.reshape((1,40,40,3))))
    ax.imshow(im, interpolation='nearest', origin='lower')
    # ax.set_aspect('auto')
    ax.text(x=0.1, y=0.1, s=f"{y:.02f}", color='white')
for ax in axs:
    ax.set_xticks([])
    ax.set_yticks([])
plt.tight_layout()

