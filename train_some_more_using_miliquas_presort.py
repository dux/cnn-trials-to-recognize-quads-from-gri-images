#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 11:50:52 2020

@author: frederic
"""
import pandas as pd
import numpy as np 
from glob import glob
from os.path import join
from keras.models import load_model 
modelsave = 'model_at_i=32.model'
model = load_model(modelsave)


posdir = '/media/frederic/kingston_data/train_synthetic_quads/synthetic_positives'
lfp  = glob(join(posdir, '*npy'))
np.random.shuffle(lfp)

N = 7
def loadimage(gripath):
    im = np.load(gripath)[5:-5, 5:-5, :]
    m, s = np.nanmean(im), np.nanstd(im)
    im[im>m+N*s] = m+N*s
    im -= np.nanmin(im)
    im /= np.nanmax(im)
    return im.astype(np.float32)
# we have this frame:
df = pd.read_csv('scores_first_part_miliquas_i=32.csv')

dfp = df[df['score']>0.5]

filenames = dfp['filename']
#%%
# from inspection, the first 1092 entries do not contain quads.
# except 113 141 213 307 842 990 which look weird
gris = []
for i in range(1092):
    if i in [113, 141, 213, 307, 842, 990]:
        continue
    gris.append(loadimage(filenames.iloc[i]))

#%%
poss = []
for e in lfp[0:len(gris)]:
    im = loadimage(e)
    poss.append(im)

poss = np.array(poss)

gris = np.array(gris)
gris[np.where(np.isnan(gris))] = 0.1 
#%%
x_train = np.concatenate((poss, gris))
y_train = np.concatenate((np.ones(poss.shape[0]), np.zeros(gris.shape[0])))
#%%
model = load_model(modelsave)
model.fit(x=x_train, y=y_train, epochs=3, validation_split=0.0)


#%%
gris = []
for i in range(6335):
    gris.append(loadimage(filenames.iloc[i]))
gris = np.array(gris)
gris[np.where(np.isnan(gris))] = 0.1 
#%%
yp = model.predict(gris)
yp = yp.flatten()
u = np.where(yp>0.5)
print(len(u[0]))
for e in u[0]:
    print(e)
    plt.title(e)
    plt.imshow(gris[e,:,:,:], origin='lower')
    plt.waitforbuttonpress()